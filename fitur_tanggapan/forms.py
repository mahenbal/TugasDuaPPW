from django import forms

class Post_Form(forms.Form) :
	error_messages = {
		'required': 'Silahkan isi input yang diminta',
	}

	description_attrs = {
		'type': 'text',
		'rows': 2,
		'class': 'form-control status-box',
		'placeholder':'Tanggapan Anda ?'
	}

	description = forms.CharField(label='', required=True, max_length=140, widget=forms.Textarea(attrs=description_attrs))
