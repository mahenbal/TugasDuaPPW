from django.db import models

# Create your models here.
class Post(models.Model):
	description = models.TextField(max_length=140)
	created_date = models.DateTimeField(auto_now_add=True)
