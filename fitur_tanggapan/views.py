from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Post_Form
from .models import Post
from django.contrib import messages

# Create your views here.
response = {}
def index(request):
	response['author'] = "Marshal Arijona Sinaga"
	post = Post.objects.all()
	response['post'] = post
	html = 'tugas_tanggapan.html'
	response['post_form'] = Post_Form
	return render(request, html, response)

def add_post(request):
	form = Post_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		post = Post(description=response['description'])
		post.save()
		messages.success(request, 'Berhasil Menanggapi')
		return HttpResponseRedirect('/tugas-tanggapan/')
	else:
		return HttpResponseRedirect('/tugas-tanggapan/')
