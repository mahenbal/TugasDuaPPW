from django.apps import AppConfig


class TugasTanggapanConfig(AppConfig):
    name = 'tugas_tanggapan'
