from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profile
from django.http import HttpRequest
import unittest

class ProfilePageUnitTest(TestCase):
    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)