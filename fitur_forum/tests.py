from django.test import TestCase, Client
from django.urls import resolve
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .views import index
from .models import Message


# Create your tests here.
class ForumTest(TestCase):
	def test_forum_url_exists(self):
		response = Client().get('/forum/')
		self.assertEqual(response.status_code, 302)

	def test_status_using_index_func(self):
		found = resolve('/forum/')
		self.assertEqual(found.func, index)




